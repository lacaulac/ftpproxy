#include  <stdio.h>
#include  <unistd.h>
#include  <sys/socket.h>
#include  <netdb.h>
#include  <string.h>
#include  <stdlib.h>
#include  <stdbool.h>
#include "client.h"

// Connexion
void c_connectSocket(clientSocket* socketc, char* serverName, char* serverPort) {
	socketc->label = "";
	socketc->isConnected = false;
	// Initailisation de hints
	memset(&(socketc->hints), 0, sizeof(socketc->hints));
	socketc->hints.ai_socktype = SOCK_STREAM;  // TCP
	socketc->hints.ai_family = AF_UNSPEC;      // les adresses IPv4 et IPv6 seront présentées par la fonction getaddrinfo
	//Récupération des informations sur le serveur
	socketc->ecode = getaddrinfo(serverName,serverPort,&(socketc->hints),&(socketc->res));
	if (socketc->ecode){
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(socketc->ecode));
		exit(1);
	}
	socketc->resPtr = socketc->res;
	while(!socketc->isConnected && socketc->resPtr!=NULL){
		//Création de la socket IPv4/TCP
		socketc->descSock = socket(socketc->resPtr->ai_family, socketc->resPtr->ai_socktype, socketc->resPtr->ai_protocol);
		if (socketc->descSock == -1) {
			perror("Erreur creation socket");
			exit(2);
		}
  		//Connexion au serveur
		socketc->ecode = connect(socketc->descSock, socketc->resPtr->ai_addr, socketc->resPtr->ai_addrlen);
		if (socketc->ecode == -1) {
			socketc->resPtr = socketc->resPtr->ai_next;    		
			close(socketc->descSock);	
		}
		// On a pu se connecter
		else socketc->isConnected = true;
	}
	freeaddrinfo(socketc->res);
	if (!socketc->isConnected){
		perror("Connexion impossible");
		exit(2);
	}
}

// Déconnexion
void c_disconnect(clientSocket* socketc) {
	//Fermeture de la socket
	close(socketc->descSock);
}

// Lecture des données
void c_read(clientSocket* socketc, char* buffer, size_t maxSize) {
	if (maxSize == 0) {
		maxSize = MAXBUFFERLEN;
	}
	memset(buffer, 0, maxSize);
	//Echange de donneés avec le serveur
	socketc->ecode = read(socketc->descSock, buffer, maxSize);
	if (socketc->ecode == -1) {perror("Problème de lecture\n"); exit(3);}
	if (socketc->ecode == 0) {perror("Connexion interrompue: "); perror(socketc->label); exit(-3);}
	socketc->buffer[socketc->ecode] = '\0';
	//printf("MESSAGE RECU DU SERVEUR: \"%s\".\n", buffer);
	printf("\e[96m[SERVEUR%s->PROXY] %s\e[0m", socketc->label, buffer);
}

// Envoi des données
void c_send(clientSocket* socketc, char* buffer, size_t maxSize) {
	write(socketc->descSock, buffer, maxSize);
	printf("\e[93m[PROXY->SERVEUR%s] %s\e[0m", socketc->label, buffer);
}
/*
int main3(int argc, char* argv[]){
	clientSocket* socketc;
	socketc = (clientSocket*)malloc(sizeof(clientSocket));
	c_connectSocket(socketc, "borasca.ovh", "8222");
	char* buffer = malloc(MAXBUFFERLEN);
	// Interaction avec le serveur
	while(socketc->isConnected) {
		c_read(socketc, buffer, 0);
		memset(buffer, 0, MAXBUFFERLEN); // Vidange du buffer
		size_t tmp = 0;
		size_t chars;
		chars = getline(&buffer, &tmp, stdin);
		//strcpy(buffer, "USER iut");
		c_send(socketc, buffer, strlen(buffer));
	}
	c_disconnect(socketc);
}*/