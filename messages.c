#include "messages.h"

void createMsg(messages* msg)
{
	msg->welcome = "220 Welcome to GreatProxy 4.20\n";
	msg->needPasswd = "331 Password required for user %s\n";
	msg->badPasswd = "530 Login incorrect.\n";
	msg->loggedIn = "230 Login successful.\n";
	msg->noPortPasv = "425 Use PORT or PASV first.\n";
	msg->portOk = "200 PORT command successful. Consider using PASV.\n";
	msg->cl_USER = "USER %s\n";
	msg->cl_PASS = "PASS %s\n";
	msg->sv_PASV = "PASV\n";
	msg->sv_SYST = "SYST\n";
	msg->bye = "221 Bye <3\n";
	msg->about = "Made with luv by Antonin VERDIER & Alexandre CLAUSSE\n";
}