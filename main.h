#ifndef HEADER_MAIN
#define HEADER_MAIN

#include "defs.h"
#include "serveur.h"
#include "client.h"

/*
//Initialement conçu pour la gestion des threads, non utilisé
typedef struct {
	serverSocket* server;
	clientSocket* client;
} dualSockets;
*/

//Permet de supprimer les caractères de retour à la ligne
void stripString(char* str)
{
	for(int i=0; i<strlen(str); i++)
	{
		if(str[i] == '\n' ||str[i] == '\r')
		{
			str[i] = '\0';
		}
	}
}

#endif