#ifndef HEADER_MESSAGES
#define HEADER_MESSAGES

typedef struct
{
	char* welcome;
	char* needPasswd;
	char* badPasswd;
	char* loggedIn;
	char* noPortPasv;
	char* portOk;
	char* cl_USER;
	char* cl_PASS;
	char* sv_PASV;
	char* sv_SYST;
	char* bye;
	char* about;
} messages;

void createMsg(messages* msg);

#endif