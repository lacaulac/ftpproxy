#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/socket.h>
#include  <netdb.h>
#include  <string.h>
#include "serveur.h"

void s_prepareSocket(serverSocket* socketS, char* serverAddr, char* serverPort)
{

	// Publication de la socket au niveau du système
	// Assignation d'une adresse IP et un numéro de port
	// Mise à zéro de hints
	memset(&(socketS->hints), 0, sizeof(socketS->hints));
	// Initailisation de hints
	socketS->hints.ai_flags = AI_PASSIVE;      // mode serveur, nous allons utiliser la fonction bind
	socketS->hints.ai_socktype = SOCK_STREAM;  // TCP
	socketS->hints.ai_family = AF_UNSPEC;      // les adresses IPv4 et IPv6 seront présentées par 
				
	// Récupération des informations du serveur
	socketS->ecode = getaddrinfo(serverAddr, serverPort, &(socketS->hints), &(socketS->res));
	if (socketS->ecode) {
		fprintf(stderr,"getaddrinfo: %s\n", gai_strerror(socketS->ecode));
		exit(1);
	}

	//Création de la socket IPv4/TCP
	socketS->descSockRDV = socket(socketS->res->ai_family, socketS->res->ai_socktype, socketS->res->ai_protocol);
	if (socketS->descSockRDV == -1) {
		perror("Erreur creation socket");
		exit(4);
	}

	// Publication de la socket
	socketS->ecode = bind(socketS->descSockRDV, socketS->res->ai_addr, socketS->res->ai_addrlen);
	if (socketS->ecode == -1) {
		perror("Erreur liaison de la socket de RDV");
		exit(3);
	}
	// Nous n'avons plus besoin de cette liste chainée addrinfo
	freeaddrinfo(socketS->res);

	socketS->len=sizeof(struct sockaddr_storage);
	socketS->ecode=getsockname(socketS->descSockRDV, (struct sockaddr *) &(socketS->myinfo), &(socketS->len));
	if (socketS->ecode == -1)
	{
		perror("SERVEUR: getsockname");
		exit(4);
	}
	socketS->ecode = getnameinfo((struct sockaddr*)&(socketS->myinfo), sizeof(socketS->myinfo), serverAddr,MAXHOSTLEN, 
                         serverPort, MAXPORTLEN, NI_NUMERICHOST | NI_NUMERICSERV); //TODO CA PLAAAAAAAAAAAAAAAAAAAANTE POUR MOI
	if (socketS->ecode != 0) {
		fprintf(stderr, "error in getnameinfo: %s\n", gai_strerror(socketS->ecode));
		exit(4);
	}
	printf("L'adresse d'ecoute est: %s\n", serverAddr);
	printf("Le port d'ecoute est: %s\n", serverPort);
	// Definition de la taille du tampon contenant les demandes de connexion
	socketS->ecode = listen(socketS->descSockRDV, LISTENLEN);
	if (socketS->ecode == -1) {
		perror("Erreur initialisation buffer d'écoute");
		exit(5);
	}

	socketS->len = sizeof(struct sockaddr_storage);
}

void s_waitForRequest(serverSocket* socketS)
{
	socketS->descSockCOM = accept(socketS->descSockRDV, (struct sockaddr *) &(socketS->from), &(socketS->len));
	if (socketS->descSockCOM == -1){
		perror("Erreur accept\n");
		exit(6);
	}
}

void s_sendData(serverSocket* socketS, char* data, size_t length)
{
	write(socketS->descSockCOM, data, length);
	printf("\e[92m[PROXY->CLIENT] %s\e[0m", data);
}

void s_readData(serverSocket* socketS, char* buffer, size_t maxSize)
{
	if(maxSize == 0) //If the size isn't specified
		maxSize = MAXBUFFERLEN;
	memset(buffer, 0, maxSize);
	read(socketS->descSockCOM, buffer, maxSize);
	printf("\e[96m[CLIENT->PROXY] %s\e[0m", buffer);
}

void s_closeSocket(serverSocket* socketS)
{
	close(socketS->descSockCOM);
	close(socketS->descSockRDV);
}

/*
int mainServeur(){
	// Attente connexion du client
	// Lorsque demande de connexion, creation d'une socket de communication avec le client
	descSockCOM = accept(descSockRDV, (struct sockaddr *) &from, &len);
	if (descSockCOM == -1){
		perror("Erreur accept\n");
		exit(6);
	}
	// Echange de données avec le client connecté
	strcpy(buffer, "BLABLABLA\n");

	write(descSockCOM, buffer, strlen(buffer));
	//Fermeture de la connexion
	close(descSockCOM);
	close(descSockRDV);
}

//*/