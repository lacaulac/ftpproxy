#ifndef HEADER_CLIENT
#define HEADER_CLIENT

#include "defs.h"

#define MAXHOSTLEN 64
#define MAXPORTLEN 6

typedef struct
{
	int descSock;                  // Descripteur de la socket
	int ecode;                     // Retour des fonctions
	struct addrinfo *res,*resPtr;  // Résultat de la fonction getaddrinfo
	struct addrinfo hints;
	char serverName[MAXHOSTLEN];   // Nom de la machine serveur
	char serverPort[MAXPORTLEN];   // Numéro de port
	char buffer[MAXBUFFERLEN];     // Buffer stockant les messages entre 
                                   // le client et le serveur
	bool isConnected;      // booléen indiquant que l'on est bien connecté
	char* label; //Label
} clientSocket;

// Connexion
void c_connectSocket(clientSocket* socketc, char* serverName, char* serverPort);

// Déconnexion
void c_disconnect(clientSocket* socketc);

// Lecture des données
void c_read(clientSocket* socketc, char* buffer, size_t maxSize);

// Envoi des données
void c_send(clientSocket* socketc, char* buffer, size_t maxSize);

#endif