#ifndef HEADER_AUTH
#define HEADER_AUTH

#define MAXBUFFERLEN 1024

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

bool rightSyntax(char *inBuffer, char *test);

char *affectLeftValue(char *inBuffer, char *test);

char *affectRightValue(char *inBuffer, char *test);

void extractAddressPort(char* src, char* address, char* port);

#endif