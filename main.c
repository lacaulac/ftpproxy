#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ioctl.h>

#include "main.h"
#include "messages.h"
#include "auth.h"

messages msgs; //Contiendra les messages à envoyer aux parties (client & serveur)

int main(int argc, char** argv)
{
	if(argc != 3)
	{
		printf("Usage: proxy <LocalServerAddress> <LocalServerPort>\n");
		//Si un nombre non valide de paramètres est fourni, ouverte sur l'adresse 127.0.0.1 au port 4566
		printf("Using default settings\n");
		char buffer[255];
		sprintf(buffer, "./%s 127.0.0.1 4566", argv[0]);
		system(buffer);
	}
	else
	{
		printf("[PROXY]: Hosting on %s:%s\n", argv[1], argv[2]);
	}

	//Création de deux buffers
	char* elBuffer = malloc(MAXBUFFERLEN);
	char* elBuffer2 = NULL; //Non initialisé car utilisé avec strtok et strstr
	
	//Initialise la structure contenant les messages
	createMsg(&msgs);

	//Connection to client part
	serverSocket server;
	clientSocket dataClient;

	//Connection to server part
	clientSocket client;
	clientSocket dataServer;


	bool quit = false; //Should quit
	bool hasDataOpened = false; //Are the data connections already established?

	struct
	{
		char* username;
		char* password;
		char* serverAddress;
		char* serverDataAddress;
		char* serverDataPort;
		char* clientDataAddress;
		char* clientDataPort;
	} userInfos;

	//Allocation des buffers pour les données utilisées par le programme
	userInfos.username = malloc(MAXBUFFERLEN);
	userInfos.password = malloc(MAXBUFFERLEN);
	userInfos.serverAddress = malloc(MAXBUFFERLEN);
	userInfos.serverDataAddress = malloc(MAXBUFFERLEN);
	userInfos.serverDataPort = malloc(MAXBUFFERLEN);
	userInfos.clientDataAddress = malloc(MAXBUFFERLEN);
	userInfos.clientDataPort = malloc(MAXBUFFERLEN);

	//Initialisation de valeurs par défaut
	//strcpy(userInfos.username, "iut");
	//strcpy(userInfos.password, "KuzcoBest123");
	strcpy(userInfos.serverAddress, "lacaulac.ovh");

	//Préparation du socket
	s_prepareSocket(&server, argv[1], argv[2]);
	s_waitForRequest(&server); //Attente de la connexion
	{//Pour plus de clarté dans le fonctionnement du code
		//Proxy-Serveur->Client: 220 Welcome message
		s_sendData(&server, msgs.welcome, strlen(msgs.welcome));

		//Client->Proxy-Serveur: USER <user>@<server>
		s_readData(&server, server.buffer, 0);
		affectLeftValue(server.buffer, " ");
		elBuffer2 = affectRightValue(server.buffer, " ");
		strcpy(elBuffer, elBuffer2);
		if(strstr(elBuffer, "@")) //Si on fournit un nom d'utilisateur accompagné d'un domaine
		{

			elBuffer2 = affectLeftValue(elBuffer, "@");
			strcpy(userInfos.username, elBuffer2);
			elBuffer2 = affectRightValue(elBuffer, "@");
			strcpy(userInfos.serverAddress, elBuffer2);
			stripString(userInfos.serverAddress);
		}
		else //Sinon on stocke juste le nom d'utilisateur et on utilise le domaine par défaut (lacaulac.ovh)
		{
			strcpy(userInfos.username, elBuffer);
		}
		printf("\e[93mTrying to connect at %s with the account %s\n\e[0m", userInfos.serverAddress, userInfos.username);

		memset(elBuffer, 0, MAXBUFFERLEN);
		sprintf(elBuffer, msgs.needPasswd, userInfos.username);

		//Proxy-Serveur->Client: 331 Password required for <user>.
		s_sendData(&server, elBuffer, strlen(elBuffer));
		
		//Client->Proxy-Serveur: PASS <pass>
		s_readData(&server, server.buffer, 0);
		affectLeftValue(server.buffer, " ");
		userInfos.password = affectRightValue(server.buffer, " ");

		//Proxy-Client-->Server: Connection @ <server>
		c_connectSocket(&client, userInfos.serverAddress, "21");

		//Server->Proxy-Client: 220 Welcome message
		c_read(&client, client.buffer, 0);

		//Proxy-Client->Server: USER <user>
		memset(elBuffer, 0, MAXBUFFERLEN);
		sprintf(elBuffer, msgs.cl_USER, userInfos.username);
		c_send(&client, elBuffer, strlen(elBuffer));

		//Server->Proxy-Client: 331 Password required for <user>.
		c_read(&client, client.buffer, 0);

		//Proxy-Client->Server: PASS <pass>
		memset(elBuffer, 0, MAXBUFFERLEN);
		sprintf(elBuffer, msgs.cl_PASS, userInfos.password);
		c_send(&client, elBuffer, strlen(elBuffer));

		//Server->Proxy-Client: 230 User <user> logged in.
		c_read(&client, client.buffer, 0);



		//Proxy-Serveur->Client: 230 User <user> logged in.
		memset(elBuffer, 0, MAXBUFFERLEN);
		if(strstr(client.buffer, "230"))
			sprintf(elBuffer, msgs.loggedIn);
		else
			sprintf(elBuffer, msgs.badPasswd);
		
		s_sendData(&server, elBuffer, strlen(elBuffer));
		while(!quit)
		{
			s_readData(&server, server.buffer, 0);
			if(strcasestr(server.buffer, "SYST")) // SYST command
			{
				printf("\e[94m[EVENT]SYST Command\e[0m\n");
				c_send(&client, msgs.sv_SYST, strlen(msgs.sv_SYST));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else if(strcasestr(server.buffer, "QUIT")) // QUIT command
			{
				printf("\e[94m[EVENT]QUIT Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, msgs.bye, strlen(msgs.bye));
				quit = true;
			}
			else if(strcasestr(server.buffer, "PWD")) // PWD command
			{
				printf("\e[94m[EVENT]PWD Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else if(strcasestr(server.buffer, "MKD")) // PWD command
			{
				printf("\e[94m[EVENT]PWD Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else if(strcasestr(server.buffer, "PORT")) // PORT command
			{
				printf("\e[94m[EVENT]PORT Command\e[0m\n");
				
				//Traitement de la commande PORT envoyée par le client
				{
					affectLeftValue(server.buffer, " ");
					elBuffer2 = affectRightValue(server.buffer, "PORT "); //Récupération de la partie droite de la commande, contenant les informations sur le port et l'adresse où la connexion active est prête
					extractAddressPort(elBuffer2, userInfos.clientDataAddress, userInfos.clientDataPort); //Extraction des données mentionnées ci-dessus
				}
				printf("[INFO] Clientside data connection: %s:%s\n", userInfos.clientDataAddress, userInfos.clientDataPort);
				c_connectSocket(&dataClient, userInfos.clientDataAddress, userInfos.clientDataPort); //Création et ouverture de la socket vers la connexion active du client
				dataClient.label = " DATA CLIENT"; //Utilisation d'un "label" de socket afin de pouvoir mieux identifier les données en transit
				//Récupération des données passives du serveur
				c_send(&client, msgs.sv_PASV, strlen(msgs.sv_PASV));
				c_read(&client, client.buffer, 0);
				//Traitement des résultats de la commande PASV envoyée au serveur
				{ //Plus ou moins la même chose que pour les données de PORT, sauf que on doit récupérer le contenu entre des parenthèses
					affectLeftValue(client.buffer, "(");
					elBuffer2 = affectRightValue(client.buffer, "(");
					char* tmp = affectLeftValue(elBuffer2, ")");
					extractAddressPort(tmp, userInfos.serverDataAddress, userInfos.serverDataPort);
				}
				printf("[INFO] Serverside data connection: %s:%s\n", userInfos.serverDataAddress, userInfos.serverDataPort);
				c_connectSocket(&dataServer, userInfos.serverDataAddress, userInfos.serverDataPort); //Création et ouverture de la socket vers la connexion passive du serveur
				dataServer.label = " DATA SERVER"; //Utilisation d'un "label" de socket afin de pouvoir mieux identifier les données en transit
				s_sendData(&server, msgs.portOk, strlen(msgs.portOk)); //On informe le client que sa commande PORT a bien été prise en compte
				hasDataOpened = true; //"Unlock" de l'utilisation de commandes transférant des données, car les sockets de données sont ouvertes
			}
			else if(strcasestr(server.buffer, "CWD")) // PWD command
			{
				printf("\e[94m[EVENT]CWD Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else if(strcasestr(server.buffer, "ABOUT")) // PWD command
			{
				printf("\e[94m[EVENT]ABOUT Command\e[0m\n");
				s_sendData(&server, msgs.about, strlen(msgs.about));
			}
			else if(strcasestr(server.buffer, "FEAT")) // PWD command
			{
				printf("\e[94m[EVENT]FEAT Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else if(strcasestr(server.buffer, "LIST")) // LIST command
			{
				printf("\e[94m[EVENT]LIST Command\e[0m\n");
				if(!hasDataOpened) //Si on tente de faire une commande LIST sans "canal" de transfert de données ouvert
				{
					printf("\e[31m[ERROR] No PORT or PASV was opened before.\e[0m\n");
					s_sendData(&server, msgs.noPortPasv, strlen(msgs.noPortPasv));
				}
				else
				{
					int bytes_available; //Stockera les octets non lus
					c_send(&client, server.buffer, strlen(server.buffer));
					c_read(&client, client.buffer, 0); //150 Here comes the directory listing.
					s_sendData(&server, client.buffer, strlen(client.buffer)); //To client: 150
					c_read(&dataServer, elBuffer, 0); // Data from server data socket
					c_send(&dataClient, elBuffer, strlen(elBuffer)); //Data to client data socket
					ioctl(dataServer.descSock,FIONREAD,&bytes_available); //Vérification de l'existence de paquet non lus sur la socket de données entre le proxy et le serveur
					while(bytes_available > 0) //Envoi des données non traitées pour le moment
					{
						c_read(&dataServer, elBuffer, 0); // Data from server data socket
						c_send(&dataClient, elBuffer, strlen(elBuffer)); //Data to client data socket
						ioctl(dataServer.descSock,FIONREAD,&bytes_available); //Même vérfication que ligne 234
					}
					c_read(&client, client.buffer, 0); //226 Directory send OK.
					s_sendData(&server, client.buffer, strlen(client.buffer));
					//Fermeture des sockets de données
					c_disconnect(&dataServer);
					c_disconnect(&dataClient);
					hasDataOpened = false; //"Lock" de l'utilisation de commandes de transfert de données, puisque les sockets de données sont fermées
				}
			}
			else if(strcasestr(server.buffer, "RETR")) // LIST command
			{
				printf("\e[94m[EVENT]RETR Command\e[0m\n");
				if(!hasDataOpened) //Si on tente de faire une commande LIST sans "canal" de transfert de données ouvert
				{
					printf("\e[31m[ERROR] No PORT or PASV was opened before.\e[0m\n");
					s_sendData(&server, msgs.noPortPasv, strlen(msgs.noPortPasv));
				}
				else
				{
					int bytes_available; //Stockera les octets non lus
					c_send(&client, server.buffer, strlen(server.buffer));
					c_read(&client, client.buffer, 0); //150 Here comes the directory listing.
					s_sendData(&server, client.buffer, strlen(client.buffer)); //To client: 150
					c_read(&dataServer, elBuffer, 0); // Data from server data socket
					c_send(&dataClient, elBuffer, strlen(elBuffer)); //Data to client data socket
					ioctl(dataServer.descSock,FIONREAD,&bytes_available); //Vérification de l'existence de paquet non lus sur la socket de données entre le proxy et le serveur
					while(bytes_available > 0) //Envoi des données non traitées pour le moment
					{
						c_read(&dataServer, elBuffer, 0); // Data from server data socket
						c_send(&dataClient, elBuffer, strlen(elBuffer)); //Data to client data socket
						ioctl(dataServer.descSock,FIONREAD,&bytes_available); //Même vérfication que ligne 264
					}
					c_read(&client, client.buffer, 0); //226 Directory send OK.
					s_sendData(&server, client.buffer, strlen(client.buffer)); //226 Directory send OK.
					//Fermeture des sockets de données
					c_disconnect(&dataServer);
					c_disconnect(&dataClient);
					hasDataOpened = false; //"Lock" de l'utilisation de commandes de transfert de données, puisque les sockets de données sont fermées
				}
			}
			else if(strcasestr(server.buffer, "TYPE")) // LIST command
			{
				printf("\e[94m[EVENT]TYPE Command\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
			else //Si la commande est inconnue, on applique le pattern général d'une commande
			{
				printf("\e[94m[EVENT]Unknown Command :o\e[0m\n");
				c_send(&client, server.buffer, strlen(server.buffer));
				c_read(&client, client.buffer, 0);
				s_sendData(&server, client.buffer, strlen(client.buffer));
			}
		}
	}
	//Fermeture des sockets principaux
	s_closeSocket(&server);
	c_disconnect(&client);

	//Libération de la mémoire allouée avec malloc
	free(elBuffer);
	free(userInfos.username);
//	free(userInfos.password);
	free(userInfos.serverAddress);
	free(userInfos.serverDataPort);
	free(userInfos.serverDataAddress);
	free(userInfos.clientDataAddress);
	free(userInfos.clientDataPort);
	printf("\e[93m");
	system("sl -e");
	return 0;
}
