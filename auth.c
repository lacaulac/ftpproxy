#include "auth.h"
bool rightSyntax(char *inBuffer, char *test) {
	return strstr(inBuffer, test);
}

char *affectLeftValue(char *inBuffer, char *test) {
	return strtok(inBuffer, test);
}

char *affectRightValue(char *inBuffer, char *test) {
	return strtok(NULL, test);
}


void extractAddressPort(char* tableau, char* address, char* pPort)
{
	char *token = strtok(tableau, ",");
	char *t[MAXBUFFERLEN];
	t[0] = token;
	int i = 1;
	while(token != NULL) {
		token = strtok(NULL, ",");
		t[i] = token;
		i++;
	}
	int port = (atoi(t[4]) * 256) + (atoi(t[5]));
	sprintf(address, "%s.%s.%s.%s", t[0], t[1], t[2], t[3]);
	sprintf(pPort, "%d", port);
}