#ifndef HEADER_SERVEUR
#define HEADER_SERVEUR

#include "defs.h"

#define LISTENLEN 1                 // Taille du tampon de demande de connexion
#define MAXHOSTLEN 64
#define MAXPORTLEN 6

typedef struct
{
	int ecode;                       // Code retour des fonctions
	char serverAddr[MAXHOSTLEN];     // Adresse du serveur
	char serverPort[MAXPORTLEN];     // Port du server
	int descSockRDV;                 // Descripteur de socket de rendez-vous
	int descSockCOM;                 // Descripteur de socket de communication
	struct addrinfo hints;           // Contrôle la fonction getaddrinfo
	struct addrinfo *res;            // Contient le résultat de la fonction getaddrinfo
	struct sockaddr_storage myinfo;  // Informations sur la connexion de RDV
	struct sockaddr_storage from;    // Informations sur le client connecté
	socklen_t len;                   // Variable utilisée pour stocker les 
				                        // longueurs des structures de socket
	char buffer[MAXBUFFERLEN];       // Tampon de communication entre le client et le serveur
} serverSocket;

void s_prepareSocket(serverSocket* socketS, char* serverAddr, char* serverPort);
void s_waitForRequest(serverSocket* socketS);
serverSocket* s_superWaitForRequest(serverSocket* socketS);
void s_sendData(serverSocket* socketS, char* data, size_t length);
void s_readData(serverSocket* socketS, char* buffer, size_t maxSize);
void s_closeSocket(serverSocket* socketS);

#endif